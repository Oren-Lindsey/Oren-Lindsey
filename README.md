# Hi, I'm Oren
I'm 14 years old and I live in North Carolina (the best state btw)

## I like to code. 
So far everything I've made is open source and you can see it on here.
I mostly make websites. (btw ![javascript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E) is my favorite language)
Here are my Github Stats:

## Stats
![Oren Lindsey's Github Stats](https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=Oren-Lindsey&theme=github)
![Top languages per repo](https://github-profile-summary-cards.vercel.app/api/cards/repos-per-language?username=oren-Lindsey&theme=github)![Top languages per commit](https://github-profile-summary-cards.vercel.app/api/cards/most-commit-language?username=oren-lindsey&theme=github)
![Stats](https://github-profile-summary-cards.vercel.app/api/cards/stats?username=oren-lindsey&theme=github)![Commits per hour](https://github-profile-summary-cards.vercel.app/api/cards/productive-time?username=oren-lindsey&theme=github)
[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=Oren-Lindsey&date_format=M%20j%5B%2C%20Y%5D&fire=FF9C00&ring=38BB50&currStreakLabel=0A4DCC&sideLabels=4B525B&sideNums=4B525B&currStreakNum=4B525B&dates=4B525B)](https://git.io/streak-stats)

## My favorite projects are: 
- [A website for my christmas list](https://github.com/Oren-Lindsey/List-website). This was the first big project I made, although I wouldn't consider it big now.
- [A login system](https://github.com/Oren-Lindsey/login). This is something I made recently.
- [wasteof-client](https://github.com/Oren-Lindsey/wasteof-client) A node.js package for [wasteof.money](https://wasteof.money)

## Tech I use:
- Currently I have a ![mac](https://img.shields.io/badge/Apple-MacBook_Pro_2015-333333?style=for-the-badge&logo=apple&logoColor=white) 15 inch Intel i7 w/ 16 gigs memory and 1 TB ssd
- I mostly code on [![Replit](https://img.shields.io/badge/replit-667881?style=for-the-badge&logo=replit&logoColor=white)](https://replit.com), but I'll switch to ![vscode](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white) one of these days
- I use ![Safari](https://img.shields.io/badge/Safari-FF1B2D?style=for-the-badge&logo=Safari&logoColor=white) for normal internet browsing and ![Chrome](https://img.shields.io/badge/Google_chrome-4285F4?style=for-the-badge&logo=Google-chrome&logoColor=white) for [![Scratch](https://img.shields.io/badge/Scratch-4D97FF?style=for-the-badge&logo=Scratch&logoColor=white)](https://scratch.mit.edu/)

## How to reach me:
- ![Github](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white) If it's about one of my coding projects, please make an issue on that project's repository.
- [![Scratch](https://img.shields.io/badge/Scratch-4D97FF?style=for-the-badge&logo=Scratch&logoColor=white)](https://scratch.mit.edu/users/scratchusername40) Where I first learned coding. Account required.
- [wasteof.money](https://wasteof.money/@ee) A social media website, made by [@jeffalo](https://github.com/jeffalo). Account required.
- [![Replit](https://img.shields.io/badge/replit-667881?style=for-the-badge&logo=replit&logoColor=white)](https://replit.com/s40) An online IDE. Comment on one of my repls. Account required

## Currently working on:
Idk how often I'll update this but 🤷‍♂️
- A ![socket.io](https://img.shields.io/badge/Socket.io-010101?&style=for-the-badge&logo=Socket.io&logoColor=white) chat website
- Dozens of other small things

*Last updated: Tue Mar 1 2022, 6:02 PM*

## Outside of coding
Outside of coding I love Elon Musk and his projects, sneakers, design, streetwear, Apple Tech, Ye music, the outdoors, food, and such. I want a Tesla when I grow up. Also I'm a christian

## Btw
I'm willing to collaborate on a website if you want. Just contact me at one of the above. Also if you need help with js, html, or css then you could ask me. I probably wouldn't be much help though

##
[![Github Profile Views](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2Foren-lindsey&count_bg=%23FF0000&title_bg=%23000000&icon=github.svg&icon_color=%23FFFFFF&title=views&edge_flat=true)](https://hits.seeyoufarm.com)
